# podstawy podstaw

ciag_znakow = "Ala ma kota 12345"
print(ciag_znakow)
# \n oznacza nową linię, \t tabulator
ciag_znakow = "Ala ma kota \n\n12345"
print(ciag_znakow)

# pojedyczne znaki i wycinki jak w listach (zero-indexed!)
print(ciag_znakow[7])
print(ciag_znakow[0:10])

zlaczenie_tekstow = "Ala ma" + "kota"   # jak listy

# ###### kod poniżej NIE BĘDZIE DZIAŁAĆ bo str jest niemodyfikowalny  #########
# ciag_znakow[0] = 'U'
# ale zadziała modyfikacja całego ciągu:
# ciag_znakow = "Ula ma kota \n\n12345"

# preprocessing
some_text = "Rok 2020 to był bardzo trudny rok"

print(some_text.lower())
# spróbuj samodzielnie .upper() .title() .capitalize()

print(len(some_text), "znaków")
# sprawdź samodzielnie len("") oraz len("    ")

wyrazy = some_text.split()     # domyślnie spacja jako znak rozdzielający

text_sentences = "First sentence. Second sentence here. Finally, the last sentence appears."

print(text_sentences.count('sentence'))

sentences_list = text_sentences.split('.')

# spróbuj samodzielnie "SEPARATOR".join(LISTA)

# można zmieniać litery
print("Ala ma kota".replace('A', 'U'))
# ale zmienia wszystkie - np.
print("Ala ma kota. A kot nie ma nikogo pod opieką.".replace('A', 'U'))

# można usuwać wszystkie znaki zamieniając na pusty string
print("Ala ma kota. A kot nie ma nikogo pod opieką...".replace('.', ''))

# inne użyteczne metody klasy str
print("   Usuwa spacje i inne białe znaki na początku   i na końcu  tekstu     ".strip())

# notacja f"" (f-stringi) oraz r"" (RAW)
wiek_Szymona = 30
wiek_Adama = 34

print(f'Szymon ma {wiek_Szymona} lat, a Adam jest o {wiek_Adama-wiek_Szymona} lat starszy')

# znak \ oznaczamy \\ (escape dla \)
print("C:\\Documents\\Subdirectrory\\file.txt")
#  albo stosujemy notację raw - wtedy Python nie interpretuje escape character
print(r"C:\Documents\Subdirectrory\file.txt")

find_one = "Cyfra (znak) 1 na początku i 1 na końcu"
print(find_one.find('1'))
print(find_one.rfind('1'))
