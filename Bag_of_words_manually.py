text_january = '''The activity in global economy continues to recover, yet economic conditions are under a
negative impact of supply-side constraints in some markets, high commodity prices and
re-escalation of the pandemic in certain countries, including the euro area. Latest forecasts
indicate that global GDP growth in 2022 will be relatively robust, although lower than in
2021. The spread of a new variant of coronavirus remains an uncertainty factor for further
course of the global epidemic and economic developments.
Energy commodity prices – in particular prices of natural gas, oil and coal – as well as
prices of some agricultural commodities run at markedly higher levels than a year ago. At
the same time, global supply chain disruptions continue and international shipping costs
are still elevated. Together with a strong recovery in demand, this contributes to a marked
rise in inflation globally, which in many countries has reached highest levels in decades.
Moreover, also inflation forecasts for the coming quarters have been revised up, which
points to the risk of longer than previously judged impact of pandemic shock on
inflationary processes.'''

text_february = '''Incoming data indicate that after a strong growth in global economic activity in 2021 , in
recent period economic conditions in some economies have deteriorated. Global economic
activity is under a negative impact of supply-side constraints in some markets, high
commodity prices and re-escalation of the pandemic. Latest forecasts indicate that global
GDP growth in 2022 will be lower than in 2021, however, it will remain relatively robust,
supported by an increase in consumption in many countries taking place amid good
situation on the labour market and continued recovery in activity after the pandemic crisis.
Further epidemic and geopolitical situation, as well as developments in economic policy
in the biggest countries remain an uncertainty factor for further course of the global
economic conditions.
Energy commodity prices – in particular prices of natural gas, oil and coal – as well as
prices of some agricultural commodities remain markedly higher than a year ago. At the
same time, global supply chain disruptions continue and international shipping costs are
still elevated. This contributes to a marked rise in inflation worldwide, which in many
countries has reached highest levels in decades. At the same time inflation forecasts have
been revised up and indicate that in many economies inflation in 2022 will remain higher
than central banks’ targets.'''

# objaśnienia
# 1. preprocessing: lowercase i usunąć znaki przestankowe
# 2. podział tekstu na wyrazy
# 3. stwórz listę wyrazów a następnie listę unikalnych wyrazów
# (sprawdź czy nie ma tam pustych ciągów znaków czyli "")
# HINT: 

lista_powtórkami = [1, 2, 3, 1, 1, 4, 2, 1]
lista_unikalnych = list(set(lista_powtórkami))   # albo tylko: set(lista_powtórkami)

# 4. dla danej listy słów unikalnych zlicz wystąpienia w tekście
# 5. umieść wyniki np. w słownikach (albo poczekaj na następne zajęcia i umieścisz je w pd.DataFrame)




