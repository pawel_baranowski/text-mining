# wyrażenia regularne (regular expressions albo RegEx)
# są obsługiwane w module re (w pakiecie standardowym, nic nie doinstalowujemy)
import re

adresy = 'Łagiewnicka, 91-456 Łódź oraz kod spoza Łodzi czyli 95-100\nMamy też kody warszawskie 00-919 oraz 00-551'

wzorzec_re = r'[0-9]{2}-[0-9]{3}'
wzorzec_re2 = r'\d{2}-\d{3}'   # to samo - spróbuj obie wersje

poszukaj_kodówpoczt = re.findall(wzorzec_re2, adresy)

# 'lepsze' split
zdanie_ze_spacjami = "A w tym   zdaniu to są   wielokrotne  spacje oraz\ttabulatory"

print(zdanie_ze_spacjami.split(' '))

# \s - biały znak, + oznacza że 1 lub więcej wystapień
print(re.split(r'\s+', zdanie_ze_spacjami))

# separator słów z daniu - kropki, przecinki? kropka lub przecinek ze spacją po?
# kropka i przecinek pod warunkiem, że nie są 'otoczone przez' cyfry?

